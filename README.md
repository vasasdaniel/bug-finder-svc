# Find the bug #

### Specification ###

You program shall search a text file for bugs and print the number of their occurrence. The bug is
drawn ASCII – style and can be found within bug.txt.
For testing purposes, we provide a text file called landscape.txt. The contents of this file is a lot
simpler than the file that we will be testing the completed program. Each occurrence of the character
pattern as specified in bug.txt is counted, except for the whitespaces contained therein.

### How does a bug look like? ###

	| |
	###O
	| |

### How does a landscape look like? ###

                                       
    | |                                
    ###O                               
    | |           | |                  
                  ###O                 
                  | |              | | 
                                   ###O
                                   | | 

### Sources ###

- Boyer Moore algorythm implementation: https://www.geeksforgeeks.org/boyer-moore-algorithm-for-pattern-searching/
- Boyer Moore algorythm:
http://www.personal.kent.edu/~rmuhamma/Algorithms/MyAlgorithms/StringMatch/boyerMoore.htm