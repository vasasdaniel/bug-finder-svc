import logging

from helpers.boyer_moore import search
from helpers.file_manger import read_file_from_resources

bug = read_file_from_resources('bug.txt')
landscape = read_file_from_resources('landscape.txt')
found_bug_cords = set()


def find_bugs():
    logging.debug("\n--------------------LANDSCAPE--------------------")
    logging.debug('\n'.join(landscape))
    bug_found = count_valid_bugs()
    print("\nFound " + str(bug_found) + " bugs in total.")


def count_valid_bugs():
    bug_numbers = 0
    for line in range(0, len(landscape) - 3):
        for b in bug:
            occurrences = search(landscape[line], b)
            if len(occurrences) == 0:
                break
            for occurrence in occurrences:
                if is_it_an_existing_bug_cord(line, occurrence):
                    logging.debug("Coordinate already part of a found bug at {}, {}. Skipping check".format(str(line), str(occurrence)))
                    continue
                if is_it_a_bug(occurrence, line):
                    bug_numbers += 1
    return bug_numbers


def is_it_a_bug(occurrence, line):
    found_bug = get_bug_cords_if_valid(occurrence, line)
    if len(found_bug) > 0:
        logging.debug("Bug found at " + str(line) + "," + str(occurrence))
        found_bug_cords.update(found_bug)
        return True
    return False


def is_it_an_existing_bug_cord(x, y):
    return (x, y) in found_bug_cords


def get_bug_cords_if_valid(mx, my):
    possible_bug = set()
    for by in range(0, len(bug)):
        for bx in range(0, len(bug[by])):
            if bug[by][bx] == landscape[my + by][mx + bx]:
                possible_bug.add((my + by, mx + bx))
            else:
                possible_bug = set()
                break
    return possible_bug


def main():
    logging.basicConfig(level=logging.INFO)
    find_bugs()


if __name__ == "__main__":
    main()
