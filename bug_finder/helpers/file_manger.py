import os


def read_file_from_resources(filename):
    file_path = os.path.join(os.path.dirname(__file__), '..', 'resources', filename)
    with open(file_path) as fp:
        return fp.read().splitlines()
